QSSI_WORKSPACE_ABS_PATH=../qssi12/android
VENDOR_WORKSPACE_ABS_PATH=.
DESTINATION_SUPER_ABS_PATH=./super_image
# for MTK
envsetup(){
	source build/envsetup.sh
	INNOSZ_PRODUCT_MSSI=sys_mssi_64_cn_armv82
	INNOSZ_PRODUCT_MSSI_PATH=out_sys/target/product/mssi_64_cn_armv82
	INNOSZ_PRODUCT_VND=vnd_D1003
	INNOSZ_PRODUCT_VND_PATH=out/target/product/k6833v1_64_k419
	OUT_DIR=out
	lunch $INNOSZ_PRODUCT_VND-userdebug
}

mssi_envsetup(){
	source build/envsetup.sh
	INNOSZ_PRODUCT_MSSI=sys_mssi_64_cn_armv82
	INNOSZ_PRODUCT_MSSI_PATH=out_sys/target/product/mssi_64_cn_armv82
	INNOSZ_PRODUCT_VND=vnd_D1003
	INNOSZ_PRODUCT_VND_PATH=out/target/product/k6833v1_64_k419
	OUT_DIR=out_sys
	lunch $INNOSZ_PRODUCT_MSSI-userdebug
}

#source build/envsetup.sh && export OUT_DIR=out_sys && lunch sys_mssi_64_cn_armv82-userdebug && make sys_images
#source build/envsetup.sh && export OUT_DIR=out && lunch vnd_k6833v1_64_k419-userdebug && make vnd_images krn_images
#python out_sys/target/product/mssi_64_cn_armv82/images/split_build.py --system-dir out_sys/target/product/mssi_64_cn_armv82/images --vendor-dir out/target/product/k6833v1_64_k419/images --kernel-dir out/target/product/k6833v1_64_k419/images --output-dir out/target/product/k6833v1_64_k419/merged

full_build()
{
    # python vendor/mediatek/proprietary/scripts/releasetools/split_build_helper.py --run full_k6833v1_64_k419-userdebug | tee build.log
    source build/envsetup.sh && export OUT_DIR=out_sys && lunch $INNOSZ_PRODUCT_MSSI-userdebug && make sys_images | tee system.log
    source build/envsetup.sh && export OUT_DIR=out && lunch $INNOSZ_PRODUCT_VND-userdebug && make vnd_images krn_images -j40 | tee vendor.log
    python $INNOSZ_PRODUCT_MSSI_PATH/images/split_build.py --system-dir $INNOSZ_PRODUCT_MSSI_PATH/images --vendor-dir $INNOSZ_PRODUCT_VND_PATH/images --kernel-dir $INNOSZ_PRODUCT_VND_PATH/images --output-dir $INNOSZ_PRODUCT_VND_PATH/merged | tee merged.log
}
system_build()
{
	  source build/envsetup.sh && export OUT_DIR=out_sys && lunch $INNOSZ_PRODUCT_MSSI-userdebug && make sys_images | tee system.log
}

vendor_build()
{
    source build/envsetup.sh && export OUT_DIR=out && lunch $INNOSZ_PRODUCT_VND-userdebug && make vnd_images krn_images -j40 | tee vendor.log
}

merged_build(){
    python $INNOSZ_PRODUCT_MSSI_PATH/images/split_build.py --system-dir $INNOSZ_PRODUCT_MSSI_PATH/images --vendor-dir $INNOSZ_PRODUCT_VND_PATH/images --kernel-dir $INNOSZ_PRODUCT_VND_PATH/images --output-dir $INNOSZ_PRODUCT_VND_PATH/merged
}

scp_build(){
	make tinysys-scp -j16
}

lk_build(){
  # include logo bin build
	make lk -j16
}

clean_dtb(){
	rm $ANDROID_PRODUCT_OUT/obj/KERNEL_OBJ/arch/arm64/boot/dts/mediatek/**
}

build_ninja()
{
	# ./prebuilts/build-tools/linux-x86/bin/ninja -f out/combined-vnd_k6833v1_64.ninja $1 -j24
    ./prebuilts/build-tools/linux-x86/bin/ninja -f out/combined-$INNOSZ_PRODUCT_VND.ninja $1 -j24
}

mssi_build_ninja()
{
	# ./prebuilts/build-tools/linux-x86/bin/ninja -f out/combined-vnd_k6833v1_64.ninja $1 -j24
    ./prebuilts/build-tools/linux-x86/bin/ninja -f out_sys/combined-$INNOSZ_PRODUCT_MSSI.ninja $1 -j24
}


gen_sp_flash_tool_pkg()
{

    if [ ! -d sp_flash_tool_pkg ];then 
    	mkdir -p sp_flash_tool_pkg
    fi

    INNOSZ_INFO=$(cat $ANDROID_PRODUCT_OUT/vendor/build.prop | grep "ro.vendor.innosz.ver" | awk -F'=' '{print $2}')
    if [[ -z "$INNOSZ_INFO" ]]; then
      ODMSZ_BUILD_DATE=$(date "+%04Y_%02m_%02d")
      INNOSZ_INFO=$ODMSZ_BUILD_DATE
      # do something with ODMSZ_BUILD_DATE
    fi
    echo "$INNOSZ_INFO"

    SOFTWARE_IN_PATH=${ANDROID_PRODUCT_OUT}/merged
    SOFTWARE_OUT_PATH=sp_flash_tool_pkg/${INNOSZ_INFO}

    if [ ! -d $SOFTWARE_OUT_PATH ];then 
    	mkdir -p $SOFTWARE_OUT_PATH
    fi

    cp ${SOFTWARE_IN_PATH}/MT6833_Android_scatter.txt $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/preloader_k6833v1_64_k419.bin $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/vbmeta.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/vbmeta_system.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/vbmeta_vendor.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/md1img.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/spmfw.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/pi_img.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/dpm.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/scp.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/sspm.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/mcupm.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/gz.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/lk.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/boot.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/boot-debug.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/dtbo.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/tee.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/logo.bin $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/super.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/userdata.img $SOFTWARE_OUT_PATH
    cp ${SOFTWARE_IN_PATH}/APDB_MT6853_S01__W2308 $SOFTWARE_OUT_PATH
}

## for qualcomm
build_target()
{
  bash build.sh -j48 dist --target_only EXPERIMENTAL_USE_OPENJDK9=1.8 | tee logs1.txt
}

build_qssi()
{
  bash build.sh -j48 dist --qssi_only EXPERIMENTAL_USE_OPENJDK9=1.8
}

build_cam_bin()
{
	/bin/bash -c "(rm -f out/target/product/${TARGET_PRODUCT}/data/camx_buildbins ) && (cp out/target/product/${TARGET_PRODUCT}/obj/DATA/camx_buildbins_intermediates/camx_buildbins out/target/product/${TARGET_PRODUCT}/data/camx_buildbins ) && (rm -f /camx_buildbins && prebuilts/python/linux-x86/2.7.5/bin/python vendor/qcom/proprietary/chi-cdk/tools/buildbins/buildbins.py --bin-path=./out/target/product/${TARGET_PRODUCT}/vendor/lib/camera,./out/target/product/${TARGET_PRODUCT}/vendor/lib64/camera --target=bin --yaml-file-name=buildbins_${TARGET_PRODUCT}.yaml )"
}


build_audio_wcd973x()
{
	build_ninja out/target/product/${TARGET_PRODUCT}/vendor/lib/modules/audio_wcd937x.ko
}

build_audio_aw87519()
{
	build_ninja out/target/product/${TARGET_PRODUCT}/vendor/lib/modules/audio_aw87519.ko
}

build_audio_machine()
{
	build_ninja out/target/product/${TARGET_PRODUCT}/vendor/lib/modules/audio_machine_${TARGET_PRODUCT}.ko
}
build_super_image()
{
  python vendor/qcom/opensource/core-utils/build/build_image_standalone.py --image super --qssi_build_path $QSSI_WORKSPACE_ABS_PATH --target_build_path $VENDOR_WORKSPACE_ABS_PATH --merged_build_path $DESTINATION_SUPER_ABS_PATH --target_lunch ${TARGET_PRODUCT}
}

build_ota_package()
{
# remove it
	python vendor/qcom/opensource/core-utils/build/build_image_standalone.py --image super --qssi_build_path ${QSSI_WORKSPACE_ABS_PATH} --target_build_path ${VENDOR_WORKSPACE_ABS_PATH} --merged_build_path ${DESTINATION_SUPER_ABS_PATH} --output_ota --target_lunch ${TARGET_PRODUCT} --skip_qiifa  2>&1 | tee build_merge_OTA_package.txt
}

build_fota_package()
{
	./out/host/linux-x86/bin/ota_from_target_files -v --block --path ${QSSI_WORKSPACE_ABS_PATH}/out/host/linux-x86/ -i 0901_merged-qssi_SQ63B_target_files.zip 0902_merged-qssi_SQ63B-target_files.zip ota_diff_update.zip
}

myfind()
{
    EXCLUDE_DIRS="out prebuilts out_sys"
    find $* -not \( -path "./$EXCLUDE_DIRS/*" -prune \)
}

gen_sm6115_xml(){
	cp rawprogram_qcm4290.xml rawprogram_sm6115.xml
	sed -i  "s/xbl.elf/sm6115\/xbl.elf/g" rawprogram_sm6115.xml
	sed -i  "s/xbl_config.elf/sm6115\/xbl_config.elf/g" rawprogram_sm6115.xml
	sed -i  "s/km41.mbn/sm6115\/km41.mbn/g" rawprogram_sm6115.xml
	sed -i  "s/tz.mbn/sm6115\/tz.mbn/g" rawprogram_sm6115.xml
	sed -i  "s/hyp.mbn/sm6115\/hyp.mbn/g" rawprogram_sm6115.xml
	sed -i  "s/devcfg.mbn/sm6115\/devcfg.mbn/g" rawprogram_sm6115.xml
}

gen_sm6115_temp_xml(){
	cat rawprogram_sm6115.xml | grep "gpt_" > rawprogram_sm6115_tmp.xml
	for i in $* ;
	do
	   cat rawprogram_sm6115.xml | grep "$i" >> rawprogram_sm6115_tmp.xml
	done
	sed -i '1i\<data>' rawprogram_sm6115_tmp.xml
	sed -i '1i\<?xml version=\"1.0\" ?>' rawprogram_sm6115_tmp.xml
	echo -n "</data>" >> rawprogram_sm6115_tmp.xml
}



