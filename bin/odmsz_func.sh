#!/bin/bash

display_red()
{
  echo -e "\033[31m$*\033[0m"
}

display_green()
{
  echo -e "\033[32m$*\033[0m"
}

display_cyan()
{
  echo -e "\033[36m$*\033[0m"
}

overlay_cp()
{
	PWD=`pwd`
  filename=`echo $1 | sed 's/\/$//'`
  fullname=$PWD/$filename
  destname=`echo ${fullname/$WORK_DIR/$QG01_DIR}`
  destdir=${destname%/*}
  if [ ! -d $destdir ];then
    mkdir -p $destdir
    echo "cp -r $fullname $destname"
	  cp -r $fullname $destname
	else
	  if [ -d $fullname ];then
		  rsync -av $fullname/* $destname
		else
			cp $fullname $destname
		fi
  fi

}

overlay_diff()
{
	PWD=`pwd`
  filename=`echo $1 | sed 's/\/$//'`
  fullname=$PWD/$filename
  destname=`echo ${fullname/$WORK_DIR/$QG01_DIR}`
  if [ ! -f $destname ] && [ ! -d $destname ];then
    echo "$destname no found"
    return
  fi
	diff -r $fullname $destname
}


overlay_meld()
{
	PWD=`pwd`
  filename=`echo $1 | sed 's/\/$//'`
  fullname=$PWD/$filename
  destname=`echo ${fullname/$WORK_DIR/$QG01_DIR}`
  if [ ! -f $destname ] && [ ! -d $destname ];then
    echo "$destname no found"
    return
  fi
	meld $fullname $destname
}

overlay_git_status()
{
	files=`git status -s | awk '{print $2}'`
	PWD=`pwd`
	for file in $files
	do
	  fullname=$PWD/$file
	  destname=`echo ${fullname/$WORK_DIR/$QG01_DIR}`
    if [ ! -f $destname ] && [ ! -d $destname ];then
	  	echo -e "\033[31mA \t$file\033[0m"
	  	continue
	  fi
	  #echo $destname $fullname
	  result=`diff -r $destname $fullname`
	  if [ "$result" == "" ];then
	  	echo -e "\033[37mS \t\033[0m$file"
	  else
	  	echo -e "\033[36mM \t$file\033[0m"
	  fi
	done
}

repair_git()
{
	cd .git
	symbols=`find . -type l`
	for symbol in $symbols
	do
		file=`basename $symbol`
		ln -sf $1/.git/$file $file
		echo $file
	done
	cd ..
}

build_kernel()
{
  CURPWD=`pwd`
  rsync -av $WORK_DIR/apps_proc/kernel/msm-3.18 $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/linux-quic/git-r5/kernel/
  rsync -av $WORK_DIR/apps_proc/kernel/msm-3.18/* $WORK_DIR/apps_proc/poky/build/tmp-glibc/work-shared/mdm9607/kernel-source/
	cd $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/linux-quic/git-r5/temp
	./run.do_compile	
	if [ x"$?" == x"0" ];then
	  ./run.do_install
		./run.do_deploy
	fi
	cd $CURPWD
}

build_lk()
{
  CURPWD=`pwd`
  # rsync Ô´Ä¿Â¼±ÈÄ¿±êÄ¿Â¼²îÒ»¼¶
  rsync -av $WORK_DIR/apps_proc/bootable/bootloader/lk $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/lk/git-r1signed/bootable/
	cd $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/lk/git-r1signed/temp
	./run.do_compile
	if [ x"$?" == x"0" ];then
		./run.do_deploy
	fi
	cd $CURPWD
}

build_lk_perf()
{
  CURPWD=`pwd`
  # rsync Ô´Ä¿Â¼±ÈÄ¿±êÄ¿Â¼²îÒ»¼¶
  rsync -av $WORK_DIR/apps_proc/bootable/bootloader/lk $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/lk/git-r1signed_perf/bootable/
	cd $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/lk/git-r1signed_perf/temp
	./run.do_compile
	if [ x"$?" == x"0" ];then
		./run.do_deploy
	fi
	cd $CURPWD
}


build_power-manager()
{
  CURPWD=`pwd`
  rsync -av $WORK_DIR/apps_proc/mdm-ss-mgr/power-manager $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/power-manager/git-r4/mdm-ss-mgr/
	cd $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/power-manager/git-r4/temp/
	./run.do_compile
	cd $CURPWD
}

build_rtl8192()
{
  CURPWD=`pwd`
  rsync -av $WORK_DIR/apps_proc/wlan/realtek/rtl8192cd_92es_qg01/drivers $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/realtek8192es/git-r0/wlan/realtek/rtl8192cd_92es_qg01
	cd $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/realtek8192es/git-r0/temp/
	./run.do_compile
	./run.do_install
	cd $CURPWD
}

build_mini-ui()
{
  CURPWD=`pwd`
  rsync -av $WORK_DIR/apps_proc/mdm-ss-mgr/mini-ui $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/mini-ui/git-r3/mdm-ss-mgr/
	cd $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/mini-ui/git-r3/temp/
	./run.do_compile
	if [ x"$?" == x"0" ];then
		./run.do_install
	fi
	cd $CURPWD
}


build_mcm-core()
{
  CURPWD=`pwd`
  rsync -av $WORK_DIR/apps_proc/mcm-core/ $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/armv7a-vfp-neon-oe-linux-gnueabi/mcm-core/git-r5/mcm-core/
	cd $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/armv7a-vfp-neon-oe-linux-gnueabi/mcm-core/git-r5/temp/
	./run.do_compile
	if [ x"$?" == x"0" ];then
		./run.do_install
	fi
	cd $CURPWD
}

build_copy_apps()
{
    cp $WORK_DIR/apps_proc/poky/build/tmp-glibc/deploy/images/mdm9607/appsboot.mbn $WORK_DIR/out/bin
    cp $WORK_DIR/apps_proc/poky/build/tmp-glibc/deploy/images/mdm9607/mdm9607-boot-2K.img $WORK_DIR/out/bin
    cp $WORK_DIR/apps_proc/poky/build/tmp-glibc/deploy/images/mdm9607/mdm9607-sysfs-2K.ubi $WORK_DIR/out/bin
}

allgrep(){
	find . -path ./poky/build -prune -o -type f | xargs grep $1
}

pokygrep()
{
	find meta* -type f | xargs grep $1
}

croot(){
	cd $WORK_DIR
}


cbuild(){
	cd $WORK_DIR/build/tool
}

cwork(){
  cd $WORK_DIR/apps_proc/poky/build/tmp-glibc/work/mdm9607-oe-linux-gnueabi/
}

cimage(){
  cd $WORK_DIR/apps_proc/poky/build/tmp-glibc/deploy/images/mdm9607/
}

## SET WORK_DIR
PWD=`pwd`
LAST=${PWD##*/}
if [[ "$LAST" != "AMSS" ]]
then
  echo -e "\033[31msource QG01/function.sh in root directory\033[0m"
  return
fi
WORK_DIR=$PWD
QG01_DIR=$WORK_DIR/QG01
echo -e "\033[32mWORK_DIR=$WORK_DIR\033[0m"
export WORK_DIR
export QG01_DIR
