top_dir=`pwd`
mtk_vendor_dir=$top_dir/vendor/mediatek
change_file=`git status -s | awk '{print $2}'`

for file in $change_file
do
	basename=`basename $file`
	extension=${file##*.}
	# echo basename=$basename
	# echo extension=$extension
	if [ $extension == "orig" -o  $extension == "rej" ];then
		rm $file
	fi
	if [ $basename == "Android.bp" -o $basename == "AndroidManifest.xml" -o $basename == "strings.xml" -o $basename == "styles.xml" ];then
		echo -e "\033[31m-----------------\033[0m"
		echo -e "\033[31m${file}\033[0m"
		echo -e "\033[41mdo it by hand\033[0m"
		continue
	fi
	absolute_file_name=$top_dir/$file
	have_find=`find $mtk_vendor_dir -name $basename`
	# 显示需要同步的MTK文件
	if [[ -n $have_find ]];then
		echo -e "\033[31m-----------------\033[0m"
		echo -e "\033[31m${absolute_file_name}\033[0m"
		echo -e "\033[35m${have_find}\033[0m"
	fi
done
