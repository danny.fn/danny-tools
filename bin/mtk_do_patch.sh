source_dir=`pwd`
# patch_dir的路径要到platform这一级
cd $1
patch_dir=`pwd`
# sort可以让补丁按顺序来打
patch_files=`find . -name "*.patch"|sort`

for file in $patch_files
do
	echo "     "
	# echo file=$file
	filename=`echo $file | sed 's/\/$//'`
	basename=`basename $filename`
	dirname=`dirname $filename`
	# echo dirname=$dirname
	# echo basename=$basename
	if [ ! -d $source_dir/$dirname ];then mkdir -p $source_dir/$dirname;fi
	cd $source_dir/$dirname
	echo -e "!!!!!!!!!!!!!!!!!!!!!enter ${source_dir}/${dirname}"
	echo "!!!!!!!!!!!!!!!!!!!!!patch -t -p1 < ${patch_dir}/${filename}"
	read -n1 -p  "Do you want to continue this [Y/N/S]?"  answer
	case  $answer in
		Y | y)
			echo  "fine,continue" ;;
		N | n)
			exit 0 ;;
		S | s)
			continue ;;
	*	)
			 echo  "error choice" ;;
	esac
	echo "---------start patch---------"
	patch -t -s -p1 < ${patch_dir}/${filename}
	if [ $? != "0" ];then
		# 提示失败的文件，需要手动合并
		echo -e "\033[31mdanny-warning $filename patch failed\033[0m"
	fi
	echo "----------end patch---------"
done

cd $source_dir
