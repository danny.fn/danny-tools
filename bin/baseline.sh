VENDOR_TAG=LA.UM.9.15.2.r1-07400-KAMORTA.QSSI13.0
AMSS_VENDOR_PATH=/pciessd4/QCM4290/snapdragon-mid-2020-iot-spf-3-0_amss_standard_oem.git/LA.UM.9.15.2
HIGH_QSSI_TAG=LA.QSSI.13.0.r1-10700-qssi.0
AMSS_HIGH_QSSI_PATH=/pciessd4/QCM4290/snapdragon-mid-2020-iot-spf-3-0_amss_standard_oem.git/LA.QSSI.13.0
LOW_QSSI_TAG=LA.QSSI.11.0.r1-19300-qssi.0
AMSS_LOW_QSSI_PATH=/pciessd4/QCM4290/snapdragon-mid-2020-iot-spf-3-0_amss_standard_oem.git/LA.QSSI.11.0

CLO_VENDOR_PATH=/harddisk8/qualcomm/clo-vendor
CLO_SYSTEM_PATH=/harddisk8/qualcomm/clo-system



process_high_qssi(){
    echo "--> Start process high vesrion qssi"
    if [[ ! -d "android" ]]; then
        echo -n "当前路径下不存在android目录,将创建android目录[y/n]:"
        read answer

        # 判断用户的输入并执行相应操作
        case "$answer" in
            [yY]|[yY][eE][sS])
                mkdir android
                # 在这里添加你想要执行的命令
                ;;
            *)
                echo "程序退出"
                return 1
                ;;
        esac
        mkdir android
    fi

    if  [ ! -d .git ];then
        echo "--> git init . "
        git init . 
    fi
    echo "--> rm -rf android/*  (y/n)"
    read answer
    rm -rf android/*

    manifest=$(readlink $CLO_SYSTEM_PATH/.repo/manifest.xml)

    if [[ $(basename "$manifest") != $HIGH_QSSI_TAG.xml ]]; then
        echo "错误: $CLO_SYSTEM_PATH当前代码不是$HIGH_QSSI_TAG,程序退出"
        return 1
    fi

    echo "--> Open source part : mv $CLO_SYSTEM_PATH/* android/ "
    read answer
    mv $CLO_SYSTEM_PATH/* android/
    

    echo "--> Open source part : mv android/prebuilts ."
    read answer
    mv android/prebuilts .
    

    echo "--> Open source part : find android -name ".git" | xargs rm -rf"
    read answer
    find android -name ".git" | xargs rm -rf 

    echo "Check QSSI TAG and AU TAG"
    line=$(head -n 1 $AMSS_HIGH_QSSI_PATH/LINUX/android/vendor/qcom/proprietary/prebuilt_HY11/AU_INFO.txt)
    echo "HIGH_QSSI_TAG:$HIGH_QSSI_TAG"
    echo "AU_INFO_TAG  :$line"
    read answer  

    echo "--> Qcom private part: cp -a $AMSS_HIGH_QSSI_PATH/LINUX/android/vendor/qcom/proprietary android/vendor/qcom/"
    read answer
    cp -a $AMSS_HIGH_QSSI_PATH/LINUX/android/vendor/qcom/proprietary android/vendor/qcom/
    
    git status android > ../process_high_qssi.txt
    echo "--> git add -f android"    
    read answer
    git add -f android
    echo "--> git commit -m \"update baseline to $HIGH_QSSI_TAG\""
    read answer
    git commit -m "update baseline to $HIGH_QSSI_TAG"
   
}

process_low_qssi(){
    echo "--> Start process low vesrion qssi"
    if [[ ! -d "android" ]]; then
        echo -n "当前路径下不存在android目录,将创建android目录[y/n]:"
        read answer

        # 判断用户的输入并执行相应操作
        case "$answer" in
            [yY]|[yY][eE][sS])
                mkdir android
                # 在这里添加你想要执行的命令
                ;;
            *)
                echo "程序退出"
                return 1
                ;;
        esac
        mkdir android
    fi

    if  [ ! -d .git ];then
        echo "--> git init . "
        git init . 
    fi
    echo "--> rm -rf android/*"
    read answer
    rm -rf android/*

    manifest=$(readlink $CLO_SYSTEM_PATH/.repo/manifest.xml)

    if [[ $(basename "$manifest") != $LOW_QSSI_TAG.xml ]]; then
        echo "错误: $CLO_SYSTEM_PATH当前代码不是$LOW_QSSI_TAG,程序退出"
        return 1
    fi

    echo "--> Open source part : mv $CLO_SYSTEM_PATH/* android/"
    read answer
    mv $CLO_SYSTEM_PATH/* android/
    

    echo "--> Open source part : mv android/prebuilts ."
    read answer
    mv android/prebuilts .
    

    echo "--> Open source part : find android -name ".git" | xargs rm -rf"
    read answer
    find android -name ".git" | xargs rm -rf 

    echo "Check QSSI TAG and AU TAG"
    line=$(head -n 1 $AMSS_LOW_QSSI_PATH/LINUX/android/vendor/qcom/proprietary/prebuilt_HY11/AU_INFO.txt)
    echo "LOW_QSSI_TAG:$LOW_QSSI_TAG"
    echo "AU_INFO_TAG  :$line"
    read answer

    echo "--> Qcom private part: cp -a $AMSS_LOW_QSSI_PATH/LINUX/android/vendor/qcom/proprietary android/vendor/qcom/"
    read answer
    cp -a $AMSS_LOW_QSSI_PATH/LINUX/android/vendor/qcom/proprietary android/vendor/qcom/
    
    git status android > ../process_low_qssi.txt
    echo "--> git add -f android"
    read answer
    git add -f android
    
    echo "--> git commit -m \"update baseline to $LOW_QSSI_TAG\""
    read answer
    git commit -m "update baseline to $LOW_QSSI_TAG"    
}

process_vendor_qssi(){

    if  [  -d .git ];then
        echo "--> mv .git ../"
        mv .git ../
    fi

    echo "--> rm -rf *"
    read answer
    rm -rf *

    manifest=$(readlink $CLO_VENDOR_PATH/.repo/manifest.xml)

    if [[ $(basename "$manifest") != $VENDOR_TAG.xml ]]; then
        echo "错误: $CLO_VENDOR_PATH当前代码不是$VENDOR_TAG,程序退出"
        return 1
    fi

    echo "--> Open source part : mv $CLO_VENDOR_PATH/* ."
    read answer
    mv $CLO_VENDOR_PATH/* .
    

    echo "--> Open source part : find . -name ".git" | xargs rm -rf"
    read answer
    find . -name ".git" | xargs rm -rf 

    echo "Check VENDOR_TAG and AU TAG"
    line=$(head -n 1 $AMSS_VENDOR_PATH/LINUX/android/vendor/qcom/proprietary/prebuilt_HY11/AU_INFO.txt)
    echo "VENDOR_TAG :$VENDOR_TAG"
    echo "AU_INFO_TAG :$line"
    read answer  

    echo "--> Qcom private part: cp -a $AMSS_VENDOR_PATH/LINUX/android/vendor/qcom/proprietary android/vendor/qcom/"
    read answer
    cp -a $AMSS_VENDOR_PATH/LINUX/android/vendor/qcom/proprietary vendor/qcom/
    
    if [ ! -d ../.git ];then
        echo "--> git init ."
        git init . 
    else
        echo "--> mv ../.git ."
        mv ../.git .
    fi
    git status . > ../process_vendor.txt
    read answer
    git add -f .
    echo "--> git commit -m \"update baseline to $VENDOR_TAG\""
    read answer
    git commit -m "update baseline to $VENDOR_TAG"
}





