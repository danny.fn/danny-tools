CLO_VENDOR_PATH=/harddisk8/qualcomm/clo-vendor
CLO_SYSTEM_PATH=/harddisk8/qualcomm/clo-system
CLO_KERNEL_PLATFORM_PATH=/harddisk8/qualcomm/clo-kernelplatform
CLO_AUDIO_PATH=/harddisk8/qualcomm/clo-techpack-audio
CLO_CAMERA_PATH=/harddisk8/qualcomm/clo-techpack-camera
CLO_DISPLAY_PATH=/harddisk8/qualcomm/clo-techpack-display
CLO_GRAPHICS_PATH=/harddisk8/qualcomm/clo-techpack-graphics
CLO_CV_PATH=/harddisk8/qualcomm/clo-techpack-cv
CLO_VIDEO_PATH=/harddisk8/qualcomm/clo-techpack-video
CLO_XR_PATH=/harddisk8/qualcomm/clo-techpack-xr
AMSS_BASELINE_PATH=/pciessd2/sm7635/snapdragon-premium-high-2023-spf-2-0_amss_standard_oem/
CLO_LE_VM_PATH=/harddisk8/qualcomm/clo-le

# Get below info from about.html  Build/label | Distro Path
VENDOR_TAG=LA.VENDOR.14.3.0.r1-17300-lanai.QSSI15.0
AMSS_VENDOR_PATH=$AMSS_BASELINE_PATH/LA.VENDOR.14.3.0

HIGH_QSSI_TAG=LA.QSSI.15.0.r1-09800-qssi.0
AMSS_HIGH_QSSI_PATH=$AMSS_BASELINE_PATH/LA.QSSI.15.0

LOW_QSSI_TAG=LA.QSSI.14.0.r1-17100-qssi.0
AMSS_LOW_QSSI_PATH=$AMSS_BASELINE_PATH/LA.QSSI.14.0

KERNEL_PLATFORM_TAG=KERNEL.PLATFORM.3.0.r1-10500-kernel.0
AMSS_KERNEL_PLATFORM_PATH=$AMSS_BASELINE_PATH/KERNEL.PLATFORM.3.0

AUDIO_TAG=AUDIO.LA.9.0.r1-07400-lanai.0
AMSS_AUDIO_PATH=$AMSS_BASELINE_PATH/AUDIO.LA.9.0

CAMERA_TAG=CAMERA.LA.4.0.r2-07000-lanai.0
AMSS_CAMERA_PATH=$AMSS_BASELINE_PATH/CAMERA.LA.4.0

DISPLAY_TAG=DISPLAY.LA.4.0.r2-07600-lanai.0
AMSS_DISPLAY_PATH=$AMSS_BASELINE_PATH/DISPLAY.LA.4.0

GRAPHICS_TAG=GRAPHICS.LA.14.0.r1-07700-lanai.0
AMSS_GRAPHICS_PATH=$AMSS_BASELINE_PATH/GRAPHICS.LA.14.0

CV_TAG=CV.LA.2.0.r1-04800-lanai.0
AMSS_CV_PATH=$AMSS_BASELINE_PATH/CV.LA.2.0

VIDEO_TAG=VIDEO.LA.4.0.r2-06100-lanai.0
AMSS_VIDEO_PATH=$AMSS_BASELINE_PATH/VIDEO.LA.4.0

XR_TAG=XR.LA.1.0.r3-03800-lanai.0
AMSS_XR_PATH=$AMSS_BASELINE_PATH/XR.LA.1.0

SENSOR_TAG=SENSORS.LA.4.0.r2-04100-lanai.0
AMSS_SENSOR_PATH=$AMSS_BASELINE_PATH/SENSORS.LA.4.0

HEXLP_TAG=HEXLP.LA.1.0.r1-01800-lanai.0
AMSS_HEXLP_PATH=$AMSS_BASELINE_PATH/HEXLP.LA.1.0

LE_VM_TAG=LE.UM.7.3.1.r1-17600-genericarmv8-64.0
AMSS_LE_VM_PATH=$AMSS_BASELINE_PATH/LE.UM.7.3.1

# $1 clo_path $2 TAG $3 AMSS_PATH
process_audio(){
	process_techpack $CLO_AUDIO_PATH $AUDIO_TAG $AMSS_AUDIO_PATH
}
process_camera(){
	process_techpack $CLO_CAMERA_PATH $CAMERA_TAG $AMSS_CAMERA_PATH
}
process_display(){
	process_techpack $CLO_DISPLAY_PATH $DISPLAY_TAG $AMSS_DISPLAY_PATH
}
process_graphics(){
	process_techpack $CLO_GRAPHICS_PATH $GRAPHICS_TAG $AMSS_GRAPHICS_PATH
}

process_cv(){
	process_techpack $CLO_CV_PATH $CV_TAG $AMSS_CV_PATH
}

process_video(){
	process_techpack $CLO_VIDEO_PATH $VIDEO_TAG $AMSS_VIDEO_PATH
}

process_xr(){
	process_techpack null $XR_TAG $AMSS_XR_PATH
}

process_sensor(){
	process_techpack null $SENSOR_TAG $AMSS_SENSOR_PATH
}

process_hexlp(){
	process_techpack null $HEXLP_TAG $AMSS_HEXLP_PATH
}

process_le_vm(){
	if  [  -d .git ];then
        echo "--> mv .git ../"
        mv .git ../
    fi

    if [ $CLO_LE_VM_PATH != "null" ];then
		manifest=$(readlink $CLO_LE_VM_PATH/.repo/manifest.xml)
		echo "manifest=$manifest"
		echo "LE_VM_TAG=$LE_VM_TAG.xml"
		if [[ $(basename "$manifest") != $LE_VM_TAG.xml ]]; then
			echo "错误: $CLO_LE_VM_PATH当前代码不是$LE_VM_TAG,程序退出"
			return 1
		fi

		echo "--> Open source part : cp -a $CLO_LE_VM_PATH/* ."
		read answer
		cp -a $CLO_LE_VM_PATH/* .
		echo "--> Open source part : find . -name ".git" | xargs rm -rf"
		read answer
		find . -name ".git" | xargs rm -rf
    fi    

    echo "--> Qcom private part: cp -a $AMSS_LE_VM_PATH/apps_proc/* ."
    read answer
    cp -a $AMSS_LE_VM_PATH/apps_proc/* .
    
    # Add display part
	echo "--> Add techpack display: cp -a $CLO_DISPLAY_PATH/* ./src/display/"
	read answer
	cp -a $CLO_DISPLAY_PATH/* ./src/display/

    echo "--> Open source part : find . -name ".git" | xargs rm -rf"
	find . -name ".git" | xargs rm -rf
	
    if [ ! -d ../.git ];then
        echo "--> git init ."
        git init . 
    else
        echo "--> mv ../.git ."
        mv ../.git .
    fi

    git status . 
	
    echo "--> git add -f ."
    read answer
    git add -f .
    echo "--> git commit -m \"update le vm to $LE_VM_TAG\""
    read answer
    git commit -m "update le vm to $LE_VM_TAG"
}

process_techpack(){
    clo_path=$1
	techpack_tag=$2
	amss_techpack_path=$3
	if  [  -d .git ];then
        echo "--> mv .git ../"
        mv .git ../
    fi
    if [ $clo_path != "null" ];then
		manifest=$(readlink $clo_path/.repo/manifest.xml)
		echo "manifest=$manifest"
		echo "techpack_tag=$techpack_tag"
		if [[ $(basename "$manifest") != $techpack_tag.xml ]]; then
			echo "错误: $clo_path当前代码不是$techpack_tag,程序退出"
			return 1
		fi

		echo "--> Open source part : cp -a $clo_path/* ."
		read answer
		cp -a $clo_path/* .
		echo "--> Open source part : find . -name ".git" | xargs rm -rf"
		read answer
		find . -name ".git" | xargs rm -rf
    fi
    echo "Check TECHPACK_TAG and AU_TAG"
    line=$(cat $amss_techpack_path/LINUX/android/snap_release.xml|xmlstarlet sel -t -v '//snap_release/image/@au_tag')
    echo "TECHPACK_TAG :$techpack_tag"
    echo "AU_INFO_TAG :$line"
    read answer  

    echo "--> Qcom private part: cp -a $amss_techpack_path/LINUX/android/vendor/qcom/proprietary vendor/qcom/"
    read answer
    cp -a $amss_techpack_path/LINUX/android/vendor/qcom/proprietary vendor/qcom/
    
    if [ ! -d ../.git ];then
        echo "--> git init ."
        git init . 
    else
        echo "--> mv ../.git ."
        mv ../.git .
    fi
	# revert vendor info
	git checkout -f vendor/qcom/proprietary/prebuilt_HY11/Android.mk
	git checkout -f vendor/qcom/proprietary/prebuilt_HY22/Android.mk
	git checkout -f vendor/qcom/proprietary/prebuilt_HY11/AU_INFO.txt
	git checkout -f vendor/qcom/proprietary/prebuilt_HY22/AU_INFO.txt
	
    git status . 
	
    echo "--> git add -f ."
    read answer
    git add -f .
    echo "--> git commit -m \"update techpack to $techpack_tag\""
    read answer
    git commit -m "update techpack to $techpack_tag"
}

process_kernel_platform(){

    if  [  -d .git ];then
        echo "--> mv .git ../"
        mv .git ../
    fi

    echo "--> rm -rf *"
    read answer
    rm -rf *

    manifest=$(readlink $CLO_KERNEL_PLATFORM_PATH/.repo/manifest.xml)

    if [[ $(basename "$manifest") != $KERNEL_PLATFORM_TAG.xml ]]; then
        echo "错误: $CLO_KERNEL_PLATFORM_PATH当前代码不是$KERNEL_PLATFORM_TAG,程序退出"
        return 1
    fi

    echo "--> Open source part : mv $CLO_KERNEL_PLATFORM_PATH/kernel_platform/* ."
    read answer
    mv $CLO_KERNEL_PLATFORM_PATH/kernel_platform/* .
    

    echo "--> Open source part : find . -name ".git" | xargs rm -rf"
    read answer
    find . -name ".git" | xargs rm -rf 

    echo "Check KERNEL_PLATFORM_TAG and AU TAG"
    line=$(head -n 1 $AMSS_KERNEL_PLATFORM_PATH/kernel_platform/qcom/proprietary/prebuilt_HY11/AU_INFO.txt)
    echo "KERNEL_PLATFORM_TAG :$KERNEL_PLATFORM_TAG"
    echo "AU_INFO_TAG :$line"
    read answer  

    echo "--> Qcom private part: cp -a $AMSS_KERNEL_PLATFORM_PATH/kernel_platform/qcom ."
    read answer
    cp -a $AMSS_KERNEL_PLATFORM_PATH/kernel_platform/qcom .
    
    if [ ! -d ../.git ];then
        echo "--> git init ."
        git init . 
    else
        echo "--> mv ../.git ."
        mv ../.git .
    fi
    git status . > ../process_kernel_platform.txt
    echo "git status . > ../process_kernel_platform.txt finish"
    echo "--> git add -f ."    
    read answer
    git add -f .
    echo "--> git commit -m \"update baseline to $KERNEL_PLATFORM_TAG\""
    read answer
    git commit -m "update baseline to $KERNEL_PLATFORM_TAG"
}


process_high_qssi(){
    echo "--> Start process high vesrion qssi"
    if [[ ! -d "android" ]]; then
        echo -n "当前路径下不存在android目录,将创建android目录[y/n]:"
        read answer

        # 判断用户的输入并执行相应操作
        case "$answer" in
            [yY]|[yY][eE][sS])
                mkdir android
                # 在这里添加你想要执行的命令
                ;;
            *)
                echo "程序退出"
                return 1
                ;;
        esac
        mkdir android
    fi

    if  [ ! -d .git ];then
        echo "--> git init . "
        git init . 
    fi
    echo "--> rm -rf android/*  (y/n)"
    read answer
    rm -rf android/*

    manifest=$(readlink $CLO_SYSTEM_PATH/.repo/manifest.xml)

    if [[ $(basename "$manifest") != $HIGH_QSSI_TAG.xml ]]; then
        echo "错误: $CLO_SYSTEM_PATH当前代码不是$HIGH_QSSI_TAG,程序退出"
        return 1
    fi

    echo "--> Open source part : mv $CLO_SYSTEM_PATH/* android/ "
    read answer
    mv $CLO_SYSTEM_PATH/* android/
    

    echo "--> Open source part : mv android/prebuilts ."
    read answer
    mv android/prebuilts .
    

    echo "--> Open source part : find android -name ".git" | xargs rm -rf"
    read answer
    find android -name ".git" | xargs rm -rf 

    echo "Check QSSI TAG and AU TAG"
    line=$(head -n 1 $AMSS_HIGH_QSSI_PATH/LINUX/android/vendor/qcom/proprietary/prebuilt_HY11/AU_INFO.txt)
    echo "HIGH_QSSI_TAG:$HIGH_QSSI_TAG"
    echo "AU_INFO_TAG  :$line"
    read answer  

    echo "--> Qcom private part: cp -a $AMSS_HIGH_QSSI_PATH/LINUX/android/vendor/qcom/proprietary android/vendor/qcom/"
    read answer
    cp -a $AMSS_HIGH_QSSI_PATH/LINUX/android/vendor/qcom/proprietary android/vendor/qcom/
    
    git status android > ../process_high_qssi.txt
    echo "--> git add -f android"    
    read answer
    git add -f android
    echo "--> git commit -m \"update baseline to $HIGH_QSSI_TAG\""
    read answer
    git commit -m "update baseline to $HIGH_QSSI_TAG"
   
}

process_low_qssi(){
    echo "--> Start process low vesrion qssi"
    if [[ ! -d "android" ]]; then
        echo -n "当前路径下不存在android目录,将创建android目录[y/n]:"
        read answer

        # 判断用户的输入并执行相应操作
        case "$answer" in
            [yY]|[yY][eE][sS])
                mkdir android
                # 在这里添加你想要执行的命令
                ;;
            *)
                echo "程序退出"
                return 1
                ;;
        esac
        mkdir android
    fi

    if  [ ! -d .git ];then
        echo "--> git init . "
        git init . 
    fi
    echo "--> rm -rf android/*"
    read answer
    rm -rf android/*

    manifest=$(readlink $CLO_SYSTEM_PATH/.repo/manifest.xml)

    if [[ $(basename "$manifest") != $LOW_QSSI_TAG.xml ]]; then
        echo "错误: $CLO_SYSTEM_PATH当前代码不是$LOW_QSSI_TAG,程序退出"
        return 1
    fi

    echo "--> Open source part : mv $CLO_SYSTEM_PATH/* android/"
    read answer
    mv $CLO_SYSTEM_PATH/* android/
    

    echo "--> Open source part : mv android/prebuilts ."
    read answer
    mv android/prebuilts .
    

    echo "--> Open source part : find android -name ".git" | xargs rm -rf"
    read answer
    find android -name ".git" | xargs rm -rf 

    echo "Check QSSI TAG and AU TAG"
    line=$(head -n 1 $AMSS_LOW_QSSI_PATH/LINUX/android/vendor/qcom/proprietary/prebuilt_HY11/AU_INFO.txt)
    echo "LOW_QSSI_TAG:$LOW_QSSI_TAG"
    echo "AU_INFO_TAG  :$line"
    read answer

    echo "--> Qcom private part: cp -a $AMSS_LOW_QSSI_PATH/LINUX/android/vendor/qcom/proprietary android/vendor/qcom/"
    read answer
    cp -a $AMSS_LOW_QSSI_PATH/LINUX/android/vendor/qcom/proprietary android/vendor/qcom/
    
    git status android > ../process_low_qssi.txt

    echo "--> git add -f android"
    read answer
    git add -f android
    
    echo "--> git commit -m \"update baseline to $LOW_QSSI_TAG\""
    read answer
    git commit -m "update baseline to $LOW_QSSI_TAG"    
}

process_vendor_target(){

    if  [  -d .git ];then
        echo "--> mv .git ../"
        mv .git ../
    fi

    echo "--> rm -rf *"
    read answer
    rm -rf *

    manifest=$(readlink $CLO_VENDOR_PATH/.repo/manifest.xml)

    if [[ $(basename "$manifest") != $VENDOR_TAG.xml ]]; then
        echo "错误: $CLO_VENDOR_PATH当前代码不是$VENDOR_TAG,程序退出"
        return 1
    fi

    echo "--> Open source part : mv $CLO_VENDOR_PATH/* ."
    read answer
    mv $CLO_VENDOR_PATH/* .
    

    echo "--> Open source part : find . -name ".git" | xargs rm -rf"
    read answer
    find . -name ".git" | xargs rm -rf 

    echo "Check VENDOR_TAG and AU TAG"
    line=$(head -n 1 $AMSS_VENDOR_PATH/LINUX/android/vendor/qcom/proprietary/prebuilt_HY11/AU_INFO.txt)
    echo "VENDOR_TAG :$VENDOR_TAG"
    echo "AU_INFO_TAG :$line"
    read answer  

    echo "--> Qcom private part: cp -a $AMSS_VENDOR_PATH/LINUX/android/vendor/qcom/proprietary vendor/qcom/"
    read answer
    cp -a $AMSS_VENDOR_PATH/LINUX/android/vendor/qcom/proprietary vendor/qcom/
    
    if [ ! -d ../.git ];then
        echo "--> git init ."
        git init . 
    else
        echo "--> mv ../.git ."
        mv ../.git .
    fi
    git status . > ../process_vendor.txt
    echo "git status . > ../process_vendor.txt finish"
    echo "--> git add -f ."
    read answer
    git add -f .
    echo "--> git commit -m \"update baseline to $VENDOR_TAG\""
    read answer
    git commit -m "update baseline to $VENDOR_TAG"
}





