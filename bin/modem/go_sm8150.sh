#!/bin/bash
#所有的目标
targets="all copy clean aop adsp modem boot tz cdsp slpi"
#所有的镜像
images="modem boot tz adsp aop cdsp"
options="clean"
bin_dir=modem_image
export MODEM_BUILD_TIME=`date +%02y%02m%02d%02H%02M`
top=`pwd`

ADSP_PROC=.
AOP_PROC=.
BOOT_IMAGES=.
BTFM_PROC=.
CDSP_PROC=.
MODEM_PROC=.
TRUSTZONE_IMAGES=.
SLPI_PROC=.
QTEE_TAS=.
COMMON=.
usage()
{
  cat <<USAGE

     Error:   Your input argument is wrong!!!


Usage:
    ./build.sh <target> [OPTIONS]

Image:
     all
        Build all the images

     modem
        Build modem_proc dir

     boot
        Build boot_images dir

     tz
        Build trustzone_images dir

     adsp
        Build adsp_proc dir

     rpm
        Build rpm_proc dir

     debug
        Build debug_image dir

     copy
        copy images to non-hlos-images dir

OPTIONS:
     clean
        clean the build

USAGE
}

#################################
update_image()
{
    cd $top/$COMMON/common/build
    #python build.py 
    python build.py
    # python build.py --multi_image
    cd $top
}


build_copy()
{
  update_image
	cp $BOOT_IMAGES/boot_images/QcomPkg/SDMPkg/855/Bin/LA/RELEASE/prog_firehose_ddr.elf $bin_dir
	cp $BOOT_IMAGES/boot_images/QcomPkg/SDMPkg/855/Bin/LA/RELEASE/prog_firehose_lite.elf $bin_dir
	cp $COMMON/common/build/ufs/gpt_*.bin $bin_dir
	cp $COMMON/common/build/ufs/rawprogram[0-9].xml $bin_dir
	cp $COMMON/common/build/ufs/patch[0-9].xml $bin_dir
	cp $BOOT_IMAGES/boot_images/QcomPkg/SDMPkg/855/Bin/LA/RELEASE//xbl.elf $bin_dir
	cp $BOOT_IMAGES/boot_images/QcomPkg/SDMPkg/855/Bin/LA/RELEASE//xbl_config.elf $bin_dir
	cp $AOP_PROC/aop_proc/build/ms/bin/AAAAANAZO//aop.mbn $bin_dir
	cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/YAQAANAA//tz.mbn $bin_dir
	cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/YAQAANAA//hyp.mbn $bin_dir
	cp $COMMON/common/build/ufs/bin/asic//NON-HLOS.bin $bin_dir
	cp $COMMON/common/build/ufs/bin//BTFM.bin $bin_dir
	cp $COMMON/common/build/bin//dspso.bin $bin_dir
	cp $QTEE_TAS/qtee_tas/build/ms/bin/YAQAANAA//km41.mbn $bin_dir
	cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/YAQAANAA/km4.mbn $bin_dir
	cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/YAQAANAA//cmnlib.mbn $bin_dir
	cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/YAQAANAA//cmnlib64.mbn $bin_dir
	cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/YAQAANAA//devcfg.mbn $bin_dir
	cp $COMMON/common/core_qupv3fw/sdm855//qupv3fw.elf $bin_dir
	cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/YAQAANAA//uefi_sec.mbn $bin_dir
	cp $BOOT_IMAGES/boot_images/QcomPkg/SDMPkg/855/Bin/LA/RELEASE//imagefv.elf $bin_dir
	cp $COMMON/common/build/bin/apdp/apdp.mbn $bin_dir
	cp $BOOT_IMAGES/boot_images/QcomPkg/Tools/binaries//logfs_ufs_8mb.bin $bin_dir
	cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/YAQAANAA//storsec.mbn $bin_dir
	cp $COMMON/common/build/bin//multi_image.mbn $bin_dir
	# cp $COMMON/common/sectools/resources/build/fileversion2//sec.dat $bin_dir
}

build_clean()
{
    git clean -dxf
    git reset --hard HEAD
}

build_all()
{
    echo "build all the images start"
    for image in $images
    do
    echo image=$image
    build_$image
    done
}

build_all_clean()
{
    echo "build all the images start"
    for image in $images
    do
    build_$image_clean
    done
}


#################################Hexagon 8.2.07
build_modem()
{
    echo "build modem_proc start"
    cd $top/$MODEM_PROC/modem_proc/build/ms
    python ./build_variant.py sm8150.gennm.prod bparams=-k
    cd $top
}

build_modem_clean()
{
    echo "Clean modem_proc start"
    cd $top/$MODEM_PROC/modem_proc/build/ms
    python ./build_variant.py sm8150.gennm.prod -clean
    cd $top
}


#################################llvm 4.0.2 done
build_boot()
{
    echo "build boot_images start"
    cd $top/$BOOT_IMAGES/boot_images/QcomPkg/
    python ./buildex.py --variant LA -r RELEASE -t SDM855Pkg,QcomToolsPkg
    cd $top
}

build_boot_clean()
{
    echo "Dont need to Clean boot_images start"
    cd $top/$BOOT_IMAGES/boot_images/QcomPkg/
    python ./buildex.py --variant LA -r RELEASE -t SDM855Pkg,QcomToolsPkg --build_flags=cleanall
    cd $top
}


#################################llvm 4.0.11 done
build_tz()
{
    echo "build_tz"
    cd $top/$TRUSTZONE_IMAGES/trustzone_images/build/ms
    python build_all.py -b TZ.XF.5.0 CHIPSET=sm8150
    cd $top
}



build_tz_clean()
{
    echo "build_tz_clean"
    cd $top/$TRUSTZONE_IMAGES/trustzone_images/build/ms
    python build_all.py -b TZ.XF.5.0 CHIPSET=sm8150 --clean
    cd $top
  
}


#################################llvm 4.0.3 done
build_aop()
{
    echo "build rpm start"
    cd $top/$AOP_PROC/
    ./aop_proc/build/build_sm8150.sh
    cd $top
}

build_aop_clean()
{
    echo "clean rpm start"
    cd $top/$AOP_PROC/
    ./aop_proc/build/build_sm8150.sh  -c
    cd $top
}

#################################8.2.05 done
build_adsp()
{
    echo "clean adsp"
    cd $top/$ADSP_PROC
    python ./adsp_proc/build/build.py -c sm8150 -o all -f ADSP
    cd $top
}

build_adsp_clean()
{
    echo "build adsp"
}



#################################8.2.05 done
build_cdsp()
{
    echo "build cdsp start"
    cd $top/$CDSP_PROC
     python ./cdsp_proc/build/build.py -c sm8150 -o all -f CDSP
    cd $top
}

build_cdsp_clean()
{
    echo "clean cdsp start"
    cd $top/$CDSP_PROC
    python ./cdsp_proc/build/build.py -c SM8150 -f CDSP -o clean
    cd $top
}

##############8.2.05 nanopb
build_slpi()
{
    echo "build slpi start"
    cd $top/$SLPI_PROC/slpi_proc/build
    python build.py -c sm8150 -o all -f TOUCH_USES_PRAM
    cd $top
}

build_slpi_clean()
{
    echo "build_slpi_clean"
}

#################################
build_debug()
{
    echo "debug don not need build"
}

build_debug_clean()
{
    echo "debug don not need build"
}

#################################
build_wcnss()
{
    echo "wcnss don not need build"
}

build_wcnss_clean()
{
    echo "wcnss don not need build, can not clean"
}



#################################


# check the targets
if [ $# -gt 0 ];then
  param_one=`echo $targets | grep $1`
  if [ -z "$param_one" ];then
      usage
      exit 1
  fi
fi

# check the option
if [ $# -gt 1 ];then
  param_two=`echo $options | grep $2`
  if [ -z "$param_two" ];then
      usage
      exit 1
  fi
fi

if [ ! -d $bin_dir ];then
mkdir $bin_dir
fi

if [ ! -d LINUX ];then
mkdir LINUX
fi

if [ $# -eq 2 ];then
build_$1_$2
else
if [ $# -eq 1 ];then
build_$1
else
build_all
fi
fi

