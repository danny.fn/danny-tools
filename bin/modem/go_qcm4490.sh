#!/bin/bash
#���е�Ŀ��
targets="all copy clean aop adsp modem boot tz cdsp slpi"
#���еľ���
images="modem boot tz adsp aop cdsp"
options="clean"
bin_dir=modem_image
export MODEM_BUILD_TIME=$(date +%02y%02m%02d%02H%02M)
top=$(pwd)

ADSP_PROC=ADSP.HT.5.7
AOP_PROC=AOP.HO.4.0
BOOT_IMAGES=BOOT.MXF.2.0
BTFM_PROC=BTFM.CMC.1.3.0
QTEE_TAS=TZ.APPS.1.18
MODEM_PROC=MPSS.DE.3.1.1
TRUSTZONE_IMAGES=TZ.XF.5.18
CPUCP_PROC=CPUCP.FW.1.0
WLAN_PROC=WLAN.HL.3.4.3
TZ_APP=TZ.APPS.1.18
COMMON=QCM4490.LA.2.0
usage() {
    cat <<USAGE

     Error:   Your input argument is wrong!!!


Usage:
    ./build.sh <target> [OPTIONS]

Image:
     all
        Build all the images

     modem
        Build modem_proc dir

     boot
        Build boot_images dir

     tz
        Build trustzone_images dir

     adsp
        Build adsp_proc dir

     rpm
        Build rpm_proc dir

     debug
        Build debug_image dir

     copy
        copy images to non-hlos-images dir

OPTIONS:
     clean
        clean the build

USAGE
}

#################################
update_image() {
    cd $top/$COMMON/common/build
    #python build.py
    python build.py
    # python build.py --multi_image
    cd $top
}

build_copy() {
    update_image
    cp $COMMON/common/build/ufs/gpt_*.bin $bin_dir
    cp $COMMON/common/build/ufs/rawprogram[0-9].xml $bin_dir
    cp $COMMON/common/build/ufs/patch[0-9].xml $bin_dir
    cp $BOOT_IMAGES/boot_images/boot/QcomPkg/SocPkg/Clarence/Bin/LAA/RELEASE//xbl_s.melf $bin_dir
    cp $BOOT_IMAGES/boot_images/boot/QcomPkg/SocPkg/Clarence/Bin/LAA/RELEASE//xbl_config.elf $bin_dir
    cp $COMMON/common/config/qti_misc//multi_image_qti.mbn $bin_dir
    cp $COMMON/common/build/bin//multi_image.mbn $bin_dir
    cp $COMMON/common/build/ufs//zeros_5sectors.bin $bin_dir
    cp $BOOT_IMAGES/boot_images/boot/QcomPkg/SocPkg/Clarence/Bin/LAA/RELEASE//uefi.elf $bin_dir
    cp $AOP_PROC/aop_proc/build/ms/bin/AAAAANAZO/clarence//aop.mbn $bin_dir
    cp $AOP_PROC/aop_proc/build/ms/bin/AAAAANAZO/clarence//aop_devcfg.mbn $bin_dir
    cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/BADAANAA//tz.mbn $bin_dir
    cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/BADAANAA//hypvm.mbn $bin_dir
    cp $COMMON/common/build/ufs/bin/asic//NON-HLOS.bin $bin_dir
    cp $COMMON/common/build/ufs/bin/asic//BTFM.bin $bin_dir
    cp $COMMON/common/build/bin//dspso.bin $bin_dir
    cp $QTEE_TAS/qtee_tas/build/ms/bin/BADAANAA//keymint.mbn $bin_dir
    cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/BADAANAA//devcfg.mbn $bin_dir
    cp $COMMON/common/core_qupv3fw/Clarence//qupv3fw.elf $bin_dir
    cp $QTEE_TAS/qtee_tas/build/ms/bin/BADAANAA//uefi_sec.mbn $bin_dir
    cp $BOOT_IMAGES/boot_images/boot/QcomPkg/SocPkg/Clarence/Bin/LAA/RELEASE//imagefv.elf $bin_dir
    cp $BOOT_IMAGES/boot_images/boot/QcomPkg/SocPkg/Clarence/Bin/LAA/RELEASE//shrm.elf $bin_dir
    #cp $APPS_PROC/apps_proc/build-qti-distro-base-debug/tmp-glibc/deploy/images/genericarmv8//vm-bootsys.img $bin_dir
    cp $CPUCP_PROC/cpucp_proc/clarence/cpucp/cpucp.elf $bin_dir
    cp $QTEE_TAS/qtee_tas/build/ms/bin/BADAANAA//featenabler.mbn $bin_dir
    cp $BOOT_IMAGES/boot_images/boot/QcomPkg/SocPkg/Clarence/Bin/LAA/RELEASE//XblRamdump.elf $bin_dir
    cp $BOOT_IMAGES/boot_images/boot/QcomPkg/QcomToolsPkg/Bin/QcomTools/RELEASE//tools.fv $bin_dir
    cp $BOOT_IMAGES/boot_images/boot/QcomPkg/Tools/binaries//logfs_ufs_8mb.bin $bin_dir
    cp $QTEE_TAS/qtee_tas/build/ms/bin/BADAANAA//storsec.mbn $bin_dir
    cp $TRUSTZONE_IMAGES/trustzone_images/build/ms/bin/BADAANAA//rtice.mbn $bin_dir
}

build_clean() {
    git clean -dxf
    git reset --hard HEAD
}

build_all() {
    echo "build all the images start"
    for image in $images; do
        echo image=$image
        build_$image
    done
}

build_all_clean() {
    echo "build all the images start"
    for image in $images; do
        build_$image_clean
    done
}

#################################Hexagon 8.2.07
build_modem() {
    echo "build modem_proc start"
    PATH=$top:$PATH
    cd $top/$MODEM_PROC/modem_proc/build/ms
    python ./build_variant.py clarence.geniot.prod bparams=-k
    cd $top
}

build_modem_clean() {
    echo "Clean modem_proc start"
    cd $top/$MODEM_PROC/modem_proc/build/ms
    python ./build_variant.py clarence.geniot.prod --clean
    cd $top
}

#################################llvm 12.0.3 done
build_boot() {
    echo "build boot_images start"
    PATH=$top:$PATH
    cd $top/$BOOT_IMAGES/
    python boot_images/boot_tools/buildex.py -t Clarence,QcomToolsPkg -r RELEASE -v LAA
    cd $top
}

build_boot_clean() {
    echo "Dont need to Clean boot_images start"
    cd $top/$BOOT_IMAGES/
    python boot_images/boot_tools/buildex.py -tClarence,QcomToolsPkg -r RELEASE -v LAA --build_??ags=cleanall
    cd $top
}

#################################llvm 4.0.11 done
build_tz() {
    echo "build_tz"
    cd $top/$TRUSTZONE_IMAGES/trustzone_images/build/ms
    python3 build_all.py CHIPSET=clarence --branch TZ.XF.5.0 --config-file build_config_deploy_clarence.xml
    cd $top
}

build_tz_clean() {
    echo "build_tz_clean"
    cd $top/$TRUSTZONE_IMAGES/trustzone_images/build/ms
    python3 build_all.py -b TZ.XF.5.0 CHIPSET=clarence --clean
    cd $top

}

#################################llvm 4.0.3 done
build_aop() {
    echo "build rpm start"
    cd $top/$AOP_PROC/
    ./aop_proc/build/build_clarence_devcfg.sh
    cd $top
}

build_aop_clean() {
    echo "clean rpm start"
    cd $top/$AOP_PROC/
    ./build_clarence_devcfg.sh -c
    cd $top
}

################################# Hexagon 8.5.07 done
build_adsp() {
    echo "build adsp"
    cd $top/$ADSP_PROC/adsp_proc/build/ms/
    python ./build_variant.py clarence.adsp.prod
    cd $top
}

build_adsp_clean() {
    echo "clean adsp"
    python ./build_variant.py clarence.adsp.prod --clean
}

#################################8.2.05 done
build_cdsp() {
    echo "build cdsp start"
}

build_cdsp_clean() {
    echo "clean cdsp start"
}

##############8.2.05 nanopb
build_slpi() {
    echo "build slpi start"
}

build_slpi_clean() {
    echo "build_slpi_clean"
}

#################################
build_debug() {
    echo "debug don not need build"
}

build_debug_clean() {
    echo "debug don not need build"
}

#################################
build_wcnss() {
    echo "wcnss don not need build"
}

build_wcnss_clean() {
    echo "wcnss don not need build, can not clean"
}

#################################

# check the targets
if [ $# -gt 0 ]; then
    param_one=$(echo $targets | grep $1)
    if [ -z "$param_one" ]; then
        usage
        exit 1
    fi
fi

# check the option
if [ $# -gt 1 ]; then
    param_two=$(echo $options | grep $2)
    if [ -z "$param_two" ]; then
        usage
        exit 1
    fi
fi

if [ ! -d $bin_dir ]; then
    mkdir $bin_dir
fi

if [ ! -d LINUX ]; then
    mkdir LINUX
fi

if [ $# -eq 2 ]; then
    build_$1_$2
else
    if [ $# -eq 1 ]; then
        build_$1
    else
        build_all
    fi
fi
