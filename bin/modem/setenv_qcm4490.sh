TOP=$(pwd)

if [ -z $MODEM_TOOLCHANS_PATH ]; then
    export MODEM_TOOLCHANS_PATH=/workspace/bin/toolchains
fi

echo "MODEM_TOOLCHANS_PATH=${MODEM_TOOLCHANS_PATH}"

if [ ! -d /pkg/qct/software/arm/RVDS ]; then
    sudo mkdir -p /pkg/qct/software/arm/RVDS
fi
cd /pkg/qct/software/arm/RVDS
if [ -d 5.01bld94 ]; then
    sudo rm 5.01bld94
fi
sudo ln -sf ${MODEM_TOOLCHANS_PATH}/arm/5.01bld94 5.01bld94

if [ ! -d /usr/local/packages/qct/software/arm/RVDS ]; then
    sudo mkdir -p /usr/local/packages/qct/software/arm/RVDS
fi
cd /usr/local/packages/qct/software/arm/RVDS/
if [ -d 5.01bld94 ]; then
    sudo rm 5.01bld94
fi
sudo ln -sf ${MODEM_TOOLCHANS_PATH}/arm/5.01bld94 5.01bld94

if [ ! -d /pkg/qct/software/arm/RVDS ]; then
    sudo mkdir -p /pkg/qct/software/arm/RVDS
fi
cd /pkg/qct/software/arm/RVDS
if [ -d 6.01bld48 ]; then
    sudo rm 6.01bld48
fi
sudo ln -sf ${MODEM_TOOLCHANS_PATH}/arm/6.01bld48 6.01bld48

if [ ! -d /pkg/qct/software/arm ]; then
    sudo mkdir -p /pkg/qct/software/arm
fi
cd /pkg/qct/software/arm
if [ -d linaro-toolchain ]; then
    sudo rm linaro-toolchain
fi
sudo ln -sf ${MODEM_TOOLCHANS_PATH}/linaro-toolchain linaro-toolchain

if [ ! -d /pkg/qct/software/hexagon/releases ]; then
    sudo mkdir -p /pkg/qct/software/hexagon/releases
fi
cd /pkg/qct/software/hexagon/releases
if [ -d tools ]; then
    sudo rm tools
fi
sudo ln -sf ${MODEM_TOOLCHANS_PATH}/HEXAGON_Tools tools

if [ ! -d /pkg/qct/software/llvm/release ]; then
    sudo mkdir -p /pkg/qct/software/llvm/release
fi
cd /pkg/qct/software/llvm/release/
if [ -d arm ]; then
    sudo rm arm
fi
sudo ln -sf ${MODEM_TOOLCHANS_PATH}/llvm arm

if [ ! -d /prj/llvm-arm/home/common/build_tools ]; then
    sudo mkdir -p /prj/llvm-arm/home/common/build_tools
fi
cd /prj/llvm-arm/home/common/build_tools/
if [ -d gcc-linaro-arm-linux-gnueabihf-4.8-2014.02_linux ]; then
    sudo rm gcc-linaro-arm-linux-gnueabihf-4.8-2014.02_linux
fi
sudo ln -sf /pkg/qct/software/arm/linaro-toolchain/arm-linux-gnueabihf/4.8-2014.02 gcc-linaro-arm-linux-gnueabihf-4.8-2014.02_linux

if [ ! -f /pkg/qct/software/python/2.7/bin ]; then
    sudo mkdir -p /pkg/qct/software/python/2.7/bin
fi
cd /pkg/qct/software/python/2.7/bin/
if [ -f python ]; then
    sudo rm python
fi
sudo ln -sf /usr/bin/python python

if [ ! -f /pkg/qct/software/boottools ]; then
    sudo mkdir -p /pkg/qct/software/boottools/
fi
cd /pkg/qct/software/boottools/
if [ ! -f dtc ]; then
    if [ ! -f /usr/bin/dtc ]; then
        echo "use sudo  apt-get install device-tree-compiler to install dtc first"
        return 1
    else
        sudo cp /usr/bin/dtc /pkg/qct/software/boottools/
    fi
fi



cd $TOP
ln -sf /usr/bin/python3 python
export ARMROOT=${MODEM_TOOLCHANS_PATH}/arm/5.01bld94
export LM_LICENSE_FILE=${ARMROOT}/license.lic
export ARM_COMPILER_PATH=$ARMROOT/bin64
export ARM_COMPILER_DEBUGGER_PATH=$ARMROOT/sw/debugger/configdb/Boards/ARM/Cortex-A8_RTSM/linux-pentium
#Replace RVCT5.01 as RVCT4, RVCT5.01 use same flags as RVCT4
export ARMTOOLS=RVCT4
export ARMLIB=$ARMROOT/lib
export ARMINCLUDE=$ARMROOT/include
export ARMINC=$ARMINCLUDE
export ARMBIN=$ARMROOT/bin64
#export ARMPATH=$ARMBIN#remove for sdm450 rpm compile
export ARMHOME=$ARMROOT
export LLVMROOT=/pkg/qct/software/llvm/release/arm/4.0.11
export LLVMBIN=$LLVMROOT/bin
export LLVMROOT_LNX_BIN=$LLVMBIN
export HEXAGON_ROOT=${MODEM_TOOLCHANS_PATH}/HEXAGON_Tools/
export HEXAGON_RTOS_RELEASE=7.2.11
export HEXAGON_Q6VERSION=v5
export HEXAGON_IMAGE_ENTRY=0x08400000

export SD_LLVM_ROOT=/pkg/qct/software/llvm/release/arm/4.0.11
export SD_LLVM_BIN=/pkg/qct/software/llvm/release/arm/4.0.11/bin
export SD_LLVM_TOOLS=/pkg/qct/software/llvm/release/arm/4.0.11/tools/bin

export PATH=/workspace/bin/openssl-1.0.1f/apps:$ARM_COMPILER_DEBUGGER_PATH:$ARM_COMPILER_PATH:$PATH
#export PATH=/workspace/bin/python-3.6.3:$PATH_A

export SECTOOLS=$TOP/common/sectoolsv2/ext/Linux/sectools
