time=$1
echo lock_me > /sys/power/wake_lock
dir=/sdcard/battery
file=$dir/$time.txt
echo file=$file
mkdir -p $dir
echo "start loging charge">$file
status=`cat /sys/class/power_supply/battery/status`
while [ $status != "Full" ]
do
	soc=`cat /sys/class/power_supply/battery/capacity`
	vol_now=`cat /sys/class/power_supply/battery/voltage_now`
	ocv=`cat /sys/class/power_supply/bms/voltage_ocv`
	temp=`cat /sys/class/power_supply/battery/temp`
	current=`cat /sys/class/power_supply/battery/current_now`
	status=`cat /sys/class/power_supply/battery/status`
	time=`date +%H-%M`
	echo "$time    $temp    $soc    $vol_now    $ocv    $current    $status"
	echo "$time    $temp    $soc    $vol_now    $ocv    $current    $status" >> $file
	sleep 60
done
status=`cat /sys/class/power_supply/battery/status`
echo "stop loging charge battery $status">>$file
echo lock_me > /sys/power/wake_unlock