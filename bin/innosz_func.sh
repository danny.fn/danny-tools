#!/bin/bash

display_red()
{
  echo -e "\033[31m$*\033[0m"
}

display_green()
{
  echo -e "\033[32m$*\033[0m"
}

display_cyan()
{
  echo -e "\033[36m$*\033[0m"
}

overlay_cp()
{
	PWD=`pwd`
  filename=`echo $1 | sed 's/\/$//'`
  fullname=$PWD/$filename
  destname=`echo ${fullname/$WORK_DIR/$BACKUP_DIR}`
  destdir=${destname%/*}
  if [ ! -d $destdir ];then
    mkdir -p $destdir
    echo "cp -r $fullname $destname"
	  cp -r $fullname $destname
	else
	  if [ -d $fullname ];then
		  rsync -av $fullname/* $destname
		else
			cp $fullname $destname
		fi
  fi

}

overlay_diff()
{
	PWD=`pwd`
  filename=`echo $1 | sed 's/\/$//'`
  fullname=$PWD/$filename
  destname=`echo ${fullname/$WORK_DIR/$BACKUP_DIR}`
  if [ ! -f $destname ] && [ ! -d $destname ];then
    echo "$destname no found"
    return
  fi
	diff -r $fullname $destname
}


overlay_meld()
{
	PWD=`pwd`
  filename=`echo $1 | sed 's/\/$//'`
  fullname=$PWD/$filename
  destname=`echo ${fullname/$WORK_DIR/$BACKUP_DIR}`
  if [ ! -f $destname ] && [ ! -d $destname ];then
    echo "$destname no found"
    return
  fi
	meld $fullname $destname
}

overlay_git_status()
{
	files=`git status -s | awk '{print $2}'`
	PWD=`pwd`
	for file in $files
	do
	  fullname=$PWD/$file
	  destname=`echo ${fullname/$WORK_DIR/$BACKUP_DIR}`
    if [ ! -f $destname ] && [ ! -d $destname ];then
	  	echo -e "\033[31mA \t$file\033[0m"
	  	continue
	  fi
	  #echo $destname $fullname
	  result=`diff -r $destname $fullname`
	  if [ "$result" == "" ];then
	  	echo -e "\033[37mS \t\033[0m$file"
	  else
	  	echo -e "\033[36mM \t$file\033[0m"
	  fi
	done
}

repair_git()
{
	cd .git
	symbols=`find . -type l`
	for symbol in $symbols
	do
		file=`basename $symbol`
		ln -sf $1/.git/$file $file
		echo $file
	done
	cd ..
}

pokygrep()
{
	find meta* -type f | xargs grep $1
}

croot(){
	cd $WORK_DIR
}

## SET WORK_DIR
# 获取当前工作目录
PWD=`pwd`
# 提取当前目录名
LAST=${PWD##*/}
# 定义工作目录
WORK_DIR=$PWD

# 打印工作目录，以绿色字体显示
echo -e "\033[32mWORK_DIR=$WORK_DIR\033[0m"

# 提示用户输入备份目录名
read -p "Please input backup dir: " BACKUP_DIR_NAME

# 构建完整的备份目录路径
BACKUP_DIR="$WORK_DIR/$BACKUP_DIR_NAME"

# 打印备份目录路径，以绿色字体显示
echo -e "\033[32mBACKUP_DIR=$BACKUP_DIR\033[0m"

# 导出工作目录和备份目录
export WORK_DIR
export BACKUP_DIR
