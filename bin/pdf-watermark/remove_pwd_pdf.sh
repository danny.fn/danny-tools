files=`find . -name "*.pdf" | tr " " "\?"`
echo -e "$files"
rm -f log.txt
for file in $files
do
	echo -e "---->file=$file"
	filename=${file%.*}
	echo -e "filename=$filename"
	echo -e "pdftk \"$file\" input_pw 5029096788 output "${filename}_remove.pdf"" >> log.txt
	pdftk "$file" input_pw 5029096788 output "${filename}_remove.pdf"
	if [ $? = "0" ];then
	    rm "$file"
	fi
done
